from abc import ABC, abstractmethod
import yaml
from pathlib import Path


class BaseScrapper(ABC):
    def __init__(self):
        ...

    def _parse(self):
        ...

    @staticmethod
    def _read_config():
        with open(Path(__file__).parents[1] /
                  'configs/parser_config.yaml') as cfg:
            data = yaml.safe_load(cfg)
        return data
